/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/types/sensor/GNSSStatus.hpp"

namespace mauve {
  namespace types {
    namespace sensor {

      std::ostream& operator<<(std::ostream& out, const GNSS_SERVICE& s) {
        switch (s) {
        case GNSS_SERVICE::GPS: out << "GPS"; break;
        case GNSS_SERVICE::GLONASS: out << "GLONASS"; break;
        case GNSS_SERVICE::COMPASS: out << "COMPASS"; break;
        case GNSS_SERVICE::GALILEO: out << "GALILEO"; break;
        }
        return out;
      }

      std::ostream& operator<<(std::ostream& out, const FIX_STATUS& s) {
        switch (s) {
        case FIX_STATUS::NO_FIX: out << "no"; break;
        case FIX_STATUS::FIX: out << "fix"; break;
        case FIX_STATUS::SBAS_FIX: out << "satellite-based"; break;
        case FIX_STATUS::GBAS_FIX: out << "ground-based"; break;
        }
        return out;
      }

      std::ostream& operator<<(std::ostream& out, const GNSSStatus& s) {
        out << s.status << " (" << s.service << ")";
        return out;
      }

    }
  }
}

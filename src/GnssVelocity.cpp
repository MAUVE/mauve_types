/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include <iomanip>
#include "mauve/types/sensor/GnssVelocity.hpp"

namespace mauve {
  namespace types {
    namespace sensor {

      std::ostream& operator<<(std::ostream& out, const GnssVelocity& s) {

        out<<"Velocity NED     : ("<<s.velocity_NED.x<<", "
           <<s.velocity_NED.y<<", "
           <<s.velocity_NED.z<<") m/s"
           <<std::endl;
        out<<"Velocity Accuracy: "<<s.speed_acc<<" m"<<std::endl;
        out<<"Motion Heading   : "<<s.motion_heading<<" deg"<<std::endl;
        out<<"Vehicle Heading  : "<<s.vehicle_heading<<" deg"<<std::endl;
        out<<"Heading Accuracy : "<<s.heading_acc<<" deg"<<std::endl;
        return out;
      }

    }
  }
}

/*
 * Copyright 2018 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_IMUSTATE_HPP
#define MAUVE_TYPES_SENSOR_IMUSTATE_HPP

#include <mauve/types/geometry_types.hpp>

namespace mauve {
  namespace types {
    namespace sensor {

      /** Imu type */
      struct ImuState
      {
        /// Timestamp in ns
        unsigned long int timestamp;

        uint32_t imu_stamp;
        uint32_t ekf_stamp;
        uint32_t mag_stamp;

        /// Orientation expressed as quaternion
        ::mauve::types::geometry::Quaternion orientation;

        /// Euler covariance [phi, theta, psi] (psi 1st rotation around z, theta 2nd rotation around y, phi 3rd rotation around x)
        double orientation_covariance[9];

        /// Angular velocity (local frame) [rad/s]
        ::mauve::types::geometry::Vector3 angular_velocity;

        /// Covariance matrix of gyrometers measurements
        double angular_velocity_covariance[9];

        /// Linear acceleration (inertial frame) [m/s2]
        ::mauve::types::geometry::Vector3 linear_acceleration;

        /// Covariance matrix of accelerometers measurements
        double linear_acceleration_covariance[9];

        /// Magnetic field [no unit] -- unit vector corresponding to the direction of magnetic field
        ::mauve::types::geometry::Vector3 magnetic_field;

        /// Covariance matrix of magnetometers measurements
        double magnetic_field_covariance[9];

        /// True if internal ekf is initialized
        bool ekf_init_valid;

        /// True if internal ekf heading is estimated
        bool ekf_heading_valid;

        /// True if all three gyrometer values are valid
        bool angular_velocity_valid;

        /// True if all three accelerometer values are valid
        bool linear_acceleration_valid;

        /// True if all three magnetometer values are valid
        bool magnetic_field_valid;

      };

      /** Stream ImuState */
      std::ostream& operator<<(std::ostream&, const ImuState& imu);

    }
  }
}

#endif // MAUVE_TYPES_SENSOR_IMUSTATE_HPP


/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_GEOMETRY_POSE_HPP
#define MAUVE_TYPES_GEOMETRY_POSE_HPP

#include <iostream>

#include "Point.hpp"
#include "Quaternion.hpp"

namespace mauve {
  namespace types {
    namespace geometry {

      /** structure of a Pose.
       * It is defined by \a position and \a orientation.
       */
      struct Pose {
        /** position, a Vector3. */
        Point position;
        /** angle. */
        Quaternion orientation;
      };

      /** Stream a Pose2D.
       */
      std::ostream& operator<< (std::ostream& out, const Pose& p);

    } // namespace geometry
  } // namespace types
} // namespace mauve

#endif // MAUVE_TYPES_GEOMETRY_POSE_HPP

/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_IMU_HPP
#define MAUVE_TYPES_SENSOR_IMU_HPP

#include <mauve/types/geometry_types.hpp>

namespace mauve {
  namespace types {
    namespace sensor {

      /** Imu type */
      struct Imu
      {
        /** Timestamp in ns */
        unsigned long int timestamp;
        /** Orientation expressed as quaternion */
        ::mauve::types::geometry::Quaternion orientation;
        /** Angular velocity (local frame) */
        ::mauve::types::geometry::Vector3 angular_velocity;
        /** Linear acceleration (inertial frame) */
        ::mauve::types::geometry::Vector3 linear_acceleration;
        /** Magnetometer */
        ::mauve::types::geometry::Vector3 magnetometer;
      };

      /** Stream Imu */
      std::ostream& operator<<(std::ostream&, const Imu& imu);

    }
  }
}

#endif // MAUVE_TYPES_SENSOR_IMU_HPP

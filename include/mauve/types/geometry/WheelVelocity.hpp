/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_GEOMETRY_WHEELVELOCITY_HPP
#define MAUVE_TYPES_GEOMETRY_WHEELVELOCITY_HPP

#include <iostream>

namespace mauve {
  namespace types {
    namespace geometry {

      /** Structure of a velocity for 2-wheels robots. */
      struct Wheel2Velocity {
        /** Left wheel velocity */
        double left;
        /** Right wheel velocity */
        double right;
      };

      /** Stream a Wheel2Velocity */
      std::ostream& operator<< (std::ostream&, const Wheel2Velocity&);

    }
  }
}

#endif // MAUVE_TYPES_GEOMETRY_WHEELVELOCITY_HPP

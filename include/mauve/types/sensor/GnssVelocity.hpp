/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_GNSSVELOCITY_HPP
#define MAUVE_TYPES_SENSOR_GNSSVELOCITY_HPP

#include <iostream>
#include <mauve/types/geometry/Vector3.hpp>

namespace mauve {
  namespace types {
    namespace sensor {

      /// Gnss Velocity data
      struct GnssVelocity
      {
        /// Timestamp
	uint64_t timestamp;
        /// Velocity expressed in NED (North-Est-Down) frame en m/s
        geometry::Vector3 velocity_NED;
        /// speed accuracy in m/s
        float speed_acc;
        /// motion heading in degree
        float motion_heading;
        /// vehicule heading in degree
        float vehicle_heading;
        /// heading accuracy in degree (both motion and vehicle)
        float heading_acc;
      };
    }
  }
}
#endif

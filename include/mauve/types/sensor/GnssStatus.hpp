/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_GNSSSTATUS2_HPP
#define MAUVE_TYPES_SENSOR_GNSSSTATUS2_HPP

#include <iostream>

namespace mauve {
  namespace types {
    namespace sensor {

      struct GnssStatus
      {
        /// GNSS iTOW [ms]
        uint32_t iTOW;
        /// GNSS fix Type, range 0..5
        uint8_t fix_type;
        /// GNSS fix Status Flags
        uint8_t flags;
        /// GNSS Additional Flags
        uint8_t flags2;
        /// Number of SVs used in Nav Solution
        uint8_t num_sv;
        /// Differential GNSS Used
        bool diff_gnss;
        /// RTK Mode
        uint8_t rtk_mode; // 0: No, 1: Float, 2: Fixed
      };

      namespace GnssFix
      {
        const uint8_t NO_FIX = 0;
        const uint8_t DEAD_RECKONING_ONLY = 1;
        const uint8_t FIX_2D = 2;
        const uint8_t FIX_3D = 3;
        const uint8_t GNSS_DEAD_RECKONING_COMBINED = 4;
        const uint8_t TIME_ONLY = 5;
      }

      namespace GnssFlags
      {
        const uint8_t GNSS_FIX_OK = 0x01;  /// i.e. within DOP & accuracy masks
        const uint8_t DIFF_SOLN   = 0x02;  /// DGPS used
        const uint8_t PSM_ENABLED = 0x04;  /// Power Save Mode is Enabled
        const uint8_t PSM_ACQUI   = 0x08;  /// Power Save Mode in Acquisition
        const uint8_t PSM_TRACK   = 0x0C;  /// Power Save Mode in Tracking
        const uint8_t PSM_OPTIM   = 0x10;  /// Power Save Mode in Optimized Tracking
        const uint8_t PSM_INACTIVE= 0x14;  /// Power Save Mode is inactive
        const uint8_t HEADING_OK  = 0x20;  /// heading of vehicle is valid
        const uint8_t CARRIER_FLOAT = 0x40;  /// Carrier phase range float solution
        const uint8_t CARRIER_FIXED = 0x80;  /// Carrier phase range fixed solution
      };

      namespace GnssFlags2
      {
        /// TODO
      };

      /** Stream a GNSS Status */
      /// TODO : std::ostream& operator<<(std::ostream&, const GnssStatus&);
    }
  }
}

#endif

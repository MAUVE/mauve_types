/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include <iomanip>
#include "mauve/types/sensor/GnssPosition.hpp"

namespace mauve {
  namespace types {
    namespace sensor {

      std::ostream& operator<<(std::ostream& out, const GnssFixType& s) {
        switch (s) {
        case GnssFixType::NO_FIX: out << "no-fix"; break;
        case GnssFixType::DEAD_RECKONING: out << "Dead-Reckoning only"; break;
        case GnssFixType::FIX_2D: out << "fix-2D"; break;
        case GnssFixType::FIX_3D: out << "fix-3D"; break;
        case GnssFixType::FIX_DEAD_RECKONING: out << "fix+dead-reckoning"; break;
        case GnssFixType::TIME_ONLY: out << "time-only"; break;
        }
        return out;
      }

      std::ostream& operator<<(std::ostream& out, const GnssPosition& s) {
        out << std::fixed<<std::setprecision(6);
        out << "Timestamp: " << s.timestamp << " s" << std::endl
            << "Longitude: "<<s.longitude<<" deg"<<std::endl
            << "Latitude : "<<s.latitude<<" deg"<<std::endl
            << "Altitude : "<<std::setprecision(3)<<s.altitude<<" m"<<std::endl
            //<< "Fix type : "<<s.fix_type<<std::endl
            << "Accuracy (h,v,t) : ("
            <<s.horizontal_acc<<" m, "
            <<s.vertical_acc<<" m)"
            //<<s.time_acc<<" ns)"
            <<std::endl;
        return out;
      }

    }
  }
}

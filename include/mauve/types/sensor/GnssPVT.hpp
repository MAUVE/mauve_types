/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_GNSSPVT_HPP
#define MAUVE_TYPES_SENSOR_GNSSPVT_HPP

#include <iostream>
#include <mauve/types/sensor/GnssTime.hpp>
#include <mauve/types/sensor/GnssStatus.hpp>

namespace mauve {
  namespace types {
    namespace sensor {

      /// GNSS time, position and velocity measurement
      /// Adapted from ublox PVT message
      struct GnssPVT
      {
        /// timestamp of the solution (epoch)
        double timestamp;
        /// GnssTime
        GnssTime time;
        /// GnssStatus
        GnssStatus status;
        /// longitude [deg / 1e-7]
        int32_t longitude;
        /// latitude [deg / 1e-7]
        int32_t latitude;
        /// altitude above ellipsoid in mm
        int32_t altitude;
        /// altitude above mean sea level [mm]
        int32_t amsl;
        /// horizontal accuracy in mm
        int32_t horizontal_acc;
        /// vertical accuracy in mm
        int32_t vertical_acc;

        /// NED north velocity [mm/s]
        int32_t velocity_N;
        /// NED east velocity [mm/s]
        int32_t velocity_E;
        /// NED down velocity [mm/s]
        int32_t velocity_D;
        /// Ground Speed (2-D) [mm/s]
        int32_t ground_speed;
        /// Heading of motion 2-D [deg / 1e-5]
        int32_t motion_heading;
        /// Speed Accuracy Estimate [mm/s]
        uint32_t speed_acc;
        /// Heading Accuracy Estimate (both motion & vehicle) [deg / 1e-5]
        uint32_t heading_acc;

        /// Position DOP [1 / 0.01]
        uint16_t p_dop;
        /// Heading of vehicle (2-D) [deg / 1e-5]
        int32_t vehicle_heading;
        /// Magnetic declination [deg / 1e-2]
        int16_t mag_dec;
        /// Magnetic declination accuracy [deg / 1e-2]
        uint16_t mag_acc;
      };

      /// Stream a Gnss PVT -- TODO
      /// std::ostream& operator<<(std::ostream&, const GnssPVT &);
    }
  }
}

#endif

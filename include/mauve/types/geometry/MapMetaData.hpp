/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_GEOMETRY_MAPMETADATA_HPP
#define MAUVE_TYPES_GEOMETRY_MAPMETADATA_HPP

#include <iostream>

#include "Pose.hpp"

namespace mauve {
  namespace types {
    namespace geometry {

      /** structure of a MetaData.
       * It is defined by
       */
      struct MapMetaData {
        struct timespec ts;

        float resolution;

        uint32_t width;

        uint32_t height;

        Pose origin;
      };

      /** Stream a MapMetaData.
       */
      std::ostream& operator<< (std::ostream& out, const MapMetaData& m);

    } // namespace geometry
  } // namespace types
} // namespace mauve

#endif // MAUVE_TYPES_GEOMETRY_MAPMETADATA_HPP

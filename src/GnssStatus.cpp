/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include <iomanip>
#include "mauve/types/sensor/GnssStatus.hpp"

namespace mauve {
  namespace types {
    namespace sensor {

      std::ostream& operator<<(std::ostream& out, const GnssStatus& s) {
        out << "Gnss fix status = ";
        if(s.fix_type == GnssFix::NO_FIX) out<<"No Fix";
        if(s.fix_type == GnssFix::DEAD_RECKONING_ONLY) out<<"Dead-Reckoning";
        if(s.fix_type == GnssFix::FIX_2D) out<<"2D Fix";
        if(s.fix_type == GnssFix::FIX_3D) out<<"3D Fix";
        if(s.fix_type == GnssFix::GNSS_DEAD_RECKONING_COMBINED)
          out<<"GNSS_DEAD_RECKONING_COMBINED";
        if(s.fix_type == GnssFix::TIME_ONLY) out<<"Time only";
        out<<std::endl;


        out << "Gnss Solution flags:"
            << "fix=" << ((s.flags&GnssFlags::GNSS_FIX_OK)!=0)
            << "| diff="<< ((s.flags&GnssFlags::DIFF_SOLN)!=0)
          //<< "| PSM="...
            << "| heading="<< ((s.flags&GnssFlags::HEADING_OK)!=0)
            << std::endl;

        out << "Carrier solution: ";
        if(s.flags & GnssFlags::CARRIER_FLOAT ) out <<"float";
        else
          if(s.flags & GnssFlags::CARRIER_FIXED) out <<"fixed";
          else out<<"no";
        out << std::endl;

        return out;
      }

    }
  }
}

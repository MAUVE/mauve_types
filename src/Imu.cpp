#include "mauve/types/sensor/Imu.hpp"

namespace mauve {
namespace types {
namespace sensor
{

  std::ostream& operator<< (std::ostream& out, const Imu& imu)
  {
    out << "Timestamp   = " << imu.timestamp << "ns" <<std::endl;
    out << "Orientation = " << imu.orientation << std::endl;
    out << "Angular velocity = " << imu.angular_velocity << std::endl;
    out << "Linear acceleration = " << imu.linear_acceleration << std::endl;
    return out;
  }

}
}
}

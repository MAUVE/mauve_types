/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_IMAGE_HPP
#define MAUVE_TYPES_SENSOR_IMAGE_HPP

#include <string>
#include <iostream>

namespace cv { class Mat; }

namespace mauve {
  namespace types {
    namespace sensor {

      /** Image encodings */
      namespace image_encodings {
        constexpr char RGB8[] = "rgb8"; /*!< classic color mode */
        constexpr char RGBA8[] = "rgba8";
        constexpr char RGB16[] = "rgb16";
        constexpr char RGBA16[] = "rgba16";
        constexpr char BGR8[] = "bgr8";
        constexpr char BGRA8[] = "bgra8";
        constexpr char BGR16[] = "bgr16";
        constexpr char BGRA16[] = "bgra16";
        constexpr char MONO8[] = "mono8"; /*!< classic grayscale mode */
        constexpr char MONO16[] = "mono16";
        // Bayer encodings
        constexpr char BAYER_RGGB8[] = "bayer_rggb8";
        constexpr char BAYER_BGGR8[] = "bayer_bggr8";
        constexpr char BAYER_GBRG8[] = "bayer_gbrg8";
        constexpr char BAYER_GRBG8[] = "bayer_grbg8";
        constexpr char BAYER_RGGB16[] = "bayer_rggb16";
        constexpr char BAYER_BGGR16[] = "bayer_bggr16";
        constexpr char BAYER_GBRG16[] = "bayer_gbrg16";
        constexpr char BAYER_GRBG16[] = "bayer_grbg16";
      }

      /** Image type */
      struct Image {
        /** color mode, a valid image_common::image_encodings */
        std::string encoding;
        /** Image width in pixels */
        unsigned int width;
        /** Image height in pixels */
        unsigned int height;
        /** An openCV image containing the pixel value */
        cv::Mat * image;

        /** Constructor */
        Image();
        /** Copy operator */
        Image& operator=(const Image&);
      };

      /** Stream an Image */
      std::ostream& operator<<(std::ostream&, const Image&);

    }
  }
}

#endif // MAUVE_TYPES_SENSOR_IMAGE_HPP

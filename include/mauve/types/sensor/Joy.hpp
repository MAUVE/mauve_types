/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_JOY_HPP
#define MAUVE_TYPES_SENSOR_JOY_HPP

#include <iostream>
#include <vector>

namespace mauve {
  namespace types {
    namespace sensor {

      /** Joy type */
      struct Joy {
        /** the axes measurements from a joystick */
        std::vector<double> axes;
        /** the buttons measurements from a joystick  */
        std::vector<int> buttons;
      };

      /** Stream a Joy */
      std::ostream& operator<<(std::ostream&, const Joy&);

    }
  }
}

#endif // MAUVE_TYPES_SENSOR_JOY_HPP

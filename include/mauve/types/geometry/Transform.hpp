/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_GEOMETRY_TRANSFORM_HPP
#define MAUVE_TYPES_GEOMETRY_TRANSFORM_HPP

#include <iostream>

#include "Vector3.hpp"
#include "Quaternion.hpp"

namespace mauve {
  namespace types {
    namespace geometry {

      /** structure of a MetaData.
       * It is defined by
       */
      struct Transform {

        Vector3 translation;

        Quaternion rotation;
      };

      /** Stream a MapMetaData.
       */
      std::ostream& operator<< (std::ostream& out, const Transform& t);

    } // namespace geometry
  } // namespace types
} // namespace mauve

#endif // MAUVE_TYPES_GEOMETRY_TRANSFORM_HPP

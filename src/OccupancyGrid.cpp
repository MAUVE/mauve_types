/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/types/geometry/OccupancyGrid.hpp"

namespace mauve {
namespace types {
namespace geometry {

std::ostream& operator<< (std::ostream& out, const OccupancyGrid& o)
{
  out << "Frame_id: " << o.frame_id << std::endl
      << "Metadata: " << o.info << std::endl;
      //<< "Data" << o.data << std::endl;
  return out;
}

}
}
}

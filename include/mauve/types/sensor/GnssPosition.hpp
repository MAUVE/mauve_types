/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_GNSSFIX_HPP
#define MAUVE_TYPES_SENSOR_GNSSFIX_HPP

#include <iostream>

namespace mauve {
  namespace types {
    namespace sensor {

      /// GNSS fix status
      enum struct GnssFixType
      {
        NO_FIX = 0,
        DEAD_RECKONING = 1,
        FIX_2D = 2,
        FIX_3D = 3,
        FIX_DEAD_RECKONING= 4,
        TIME_ONLY = 5,
      };

      /// GNSS time an position measurement
      struct GnssPosition
      {
        /// timestamp of the solution (epoch)
        uint64_t timestamp;
        /// longitude in degree
        double longitude;
        /// latitude in degree
        double latitude;
        /// altitude above ellipsoid in m
        double altitude;
        /// Gnss fix type
        //GnssFixType fix_type;
        // time accuracy in ns
        //int time_acc;
        /// horizontal accuracy in m
        float horizontal_acc;
        /// vertical accuracy in m
        float vertical_acc;
      };

      /// Stream a Gnss Fix Type
      std::ostream& operator<<(std::ostream&, const GnssFixType &);
      /// Stream a Gnss Position
      std::ostream& operator<<(std::ostream&, const GnssPosition &);

    }
  }
}

#endif
